# Fachgruppenraum Hygienekonzept

Hygienekonzept für den Fachgruppenraum der Informatik

Pad: https://pad.gwdg.de/q5K0ZKZoRCephSHjXjzktQ#

Hygienekonzept beschlossen in der [Klausurtagung](https://pad.gwdg.de/rLN9sjlxRXiuwN37CiIK2g) vom 21.10.2020.

# PDF erstellen

Einfach die folgenden Befehle ausführen:

	git clone https://gitlab.gwdg.de/GAUMI-fginfo/fachgruppenraum-hygienekonzept.git
	cd fachgruppenraum-hygienekonzept
	make

Die `Hygienekonzept.pdf` Datei sollte sich dann im gleichen Ordner befinden.
