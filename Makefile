TARGETS=Hygienekonzept.pdf
#TARGETS=$(shell find .  -maxdepth 1 | sed 's,^./,,g' | grep \.pdf | tr '\n' ' ')
TEMPLATELATEX=~/stuff/makefiles/default.latex

LANG=de-DE

FONTSIZE=12pt
DATE="\today"
AUTHOR=Fachgruppe Informatik
PAPERSIZE=a4

VARS=--variable lang=$(LANG) --variable fontsize=$(FONTSIZE) --variable date=$(DATE) --variable papersize=$(PAPERSIZE)
#VARS=--variable lang=$(LANG) --variable fontsize=$(FONTSIZE) --variable author=$(AUTHOR) --variable date=$(DATE) --variable papersize=$(PAPERSIZE)
#ARGS= -s --latex-engine=xelatex $(VARS) --toc --variable title="$(subst .pdf,,$@ )"  
ARGS= -s --template=default.latex $(VARS) -f markdown -t latex --toc --variable title="$(subst _, ,$(subst .tex,,$@))" 

default: $(TARGETS)

all: $(shell find .  -maxdepth 1 | sed 's,^./,,g' | grep \.md | tr '\n' ' ' | sed 's,\.md,\.pdf,g') $(shell find .  -maxdepth 1 | sed 's,^./,,g' | grep \.tex | tr '\n' ' ' | sed 's,\.tex,\.pdf,g')

clean:
	rm -f *.log *.out *.aux *.toc
	#rm -f *.tex *.log *.out *.aux *.toc
	rm -f default.latex


cleanall: clean
	#rm -f *.pdf
	rm -f $(shell find .  -maxdepth 1 | sed 's,^./,,g' | grep \.md | tr '\n' ' ' | sed 's,\.md,\.tex,g')
	rm -f $(shell find .  -maxdepth 1 | sed 's,^./,,g' | grep \.tex | tr '\n' ' ' | sed 's,\.tex,\.pdf,g')
	rm -f $(shell find .  -maxdepth 1 | sed 's,^./,,g' | grep \.md | tr '\n' ' ' | sed 's,\.md,\.pdf,g')

#%.pdf: %.md
#	cat $(TEMPLATELATEX) | sed 's,\$$author\$$,$(AUTHOR),g' > default.latex
#	pandoc "$^" $(ARGS) -o "$(subst .pdf,,$@).tex"
#	# iconv -t utf-8 $^ | pandoc $(ARGS) -o $(subst .pdf,.tex,$@ )
#	pdflatex $(subst .pdf,.tex,$@)
#	pdflatex $(subst .pdf,.tex,$@)
#	rm -f $(subst .pdf,.tex,$@) $(subst .pdf,.log,$@) $(subst .pdf,.out,$@) $(subst .pdf,.aux,$@) $(subst .pdf,.toc,$@) default.latex


%.pdf: %.tex
	pdflatex $(subst .pdf,.tex,$@)
	pdflatex $(subst .pdf,.tex,$@)
	#rm -f $(subst .pdf,.tex,$@) $(subst .pdf,.log,$@) $(subst .pdf,.out,$@) $(subst .pdf,.aux,$@) $(subst .pdf,.toc,$@) 
	rm -f $(subst .pdf,.log,$@) $(subst .pdf,.out,$@) $(subst .pdf,.aux,$@) $(subst .pdf,.toc,$@) 


%.tex: %.md
	cat $(TEMPLATELATEX) | sed 's,\$$author\$$,$(AUTHOR),g' > default.latex
	#pandoc "$^" $(ARGS) -o "$@"
	pandoc "$<" $(ARGS) -o "$@"
	# iconv -t utf-8 $^ | pandoc $(ARGS) -o $(subst .pdf,.tex,$@ )
	rm -f default.latex
